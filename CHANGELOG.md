# Changelog

## 1.0.0

* Make n-ary operators for sum and product nodes

## 0.1.0

* Add first test

## 0.0.0

* First release
